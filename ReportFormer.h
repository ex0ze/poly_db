#ifndef REPORTFORMER_H
#define REPORTFORMER_H

#include <QObject>
#include <QDateTime>
#include <QRegularExpression>
#include "RegexSplitter.h"
#include "SQLClient.h"



class ReportFormer : public QObject
{
    Q_OBJECT
public:
    explicit ReportFormer(QObject *parent = nullptr);
    enum ReportType {
        RT_PATIENTS,
        RT_DOCTORS,
        RT_DISEASES
    };

    SQLClient *sql() const;
    void setSql(SQLClient *sql);
    QString container() const;
    void createReport(ReportType t);
signals:

public slots:

private:
    SQLClient* m_sql;
    QString m_container;
    ReportType m_type;
};

#endif // REPORTFORMER_H
