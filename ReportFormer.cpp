#include "ReportFormer.h"

ReportFormer::ReportFormer(QObject *parent) : QObject(parent)
{

}

SQLClient *ReportFormer::sql() const
{
    return m_sql;
}

void ReportFormer::setSql(SQLClient *sql)
{
    m_sql = sql;
}

QString ReportFormer::container() const
{
    return m_container;
}

void ReportFormer::createReport(ReportFormer::ReportType t)
{
    m_type = t;
    QRegularExpression re;
    if (t == ReportType::RT_PATIENTS) {
        m_container += "Отчёт о пациентах от " + QDate::currentDate().toString() + " " + QTime::currentTime().toString() +"\n";
        QString query = "SELECT COUNT(*) FROM Patients";
        re.setPattern("COUNT\\(\\*\\)=(\\d+)");
        int pCount = re.match(m_sql->executeQueryBlock(query)).captured(1).toInt();
        m_container += "Количество пациентов: " + QString::number(pCount) + "\n";
        query = "SELECT * FROM Patients";
        auto resp = m_sql->executeQueryBlock(query);
        re.setPattern("ID=(\\d+)\nFull_name=(.*)\nAddress=(.*)\nDisease_time=(.*)\nDisease_id=(.*)\nDoctor_id=(.*)\n");
        auto matches = re.globalMatch(resp);
        while (matches.hasNext()) {
            auto match = matches.next();
            m_container += QString(20,'=');
            m_container += "\nФИО пациента: " + match.captured(2) + "\n";
            m_container += "Адрес пациента: " + match.captured(3) + "\n";
            m_container += "Дата заболевания: " + match.captured(4) + "\n";
            m_container += "Заболевание: ";
            query = "SELECT Name FROM Diseases WHERE ID = " + QString::number(match.captured(5).toInt());
            resp = m_sql->executeQueryBlock(query);
            re.setPattern("Name=((?s).*)");
            auto diseaseMatch = re.match(resp);
            if (diseaseMatch.hasMatch()) {
                m_container += diseaseMatch.captured(1);
            }
            else m_container += "Неизвестно";
            m_container += "\nНаходится на лечении у: ";
            query = "SELECT Full_name FROM Doctors WHERE ID = " + QString::number(match.captured(6).toInt());
            resp = m_sql->executeQueryBlock(query);
            re.setPattern("Full_name=(.*)");
            auto docMatch = re.match(resp);
            if (docMatch.hasMatch()) {
                m_container += docMatch.captured(1);
            }
            else m_container += "Доктор не назначен";
            m_container += "\n";
        }
    }
    else if (t == ReportType::RT_DOCTORS) {
        m_container += "Отчёт о врачах от " + QDate::currentDate().toString() + " " + QTime::currentTime().toString() +"\n";
        QString query = "SELECT COUNT(*) FROM Doctors";
        re.setPattern("COUNT\\(\\*\\)=(\\d+)");
        int dCount = re.match(m_sql->executeQueryBlock(query)).captured(1).toInt();
        m_container += "Количество врачей: " + QString::number(dCount) + "\n";
        query = "SELECT * FROM Doctors";
        auto resp = m_sql->executeQueryBlock(query);
        re.setPattern("ID=(\\d+)\nFull_name=(.*)\nCab_num=(.*)\nPoly_num=(.*)\nWork_time=(.*)");
        auto matches = re.globalMatch(resp);
        while (matches.hasNext()) {
            auto match = matches.next();
            m_container += QString(20,'=');
            m_container += "\nФИО врача: " + match.captured(2) + "\n";
            m_container += "Номер кабинета врача: " + match.captured(3) + "\n";
            m_container += "Номер участка поликлиники: " + match.captured(4) + "\n";
            m_container += "Время работы врача:\n";
            auto wTimeList = match.captured(5).split(';');
            for (const auto &it : wTimeList) {
                m_container += it + "\n";
            }
            m_container += "Больные, находящиеся на лечении у врача:\n";
            int currentDocID = match.captured(1).toInt();
            re.setPattern("COUNT\\(\\*\\)=(\\d+)");
            query = "SELECT COUNT(*) FROM Patients WHERE Doctor_id = " + match.captured(1);
            resp = m_sql->executeQueryBlock(query);
            int pCount = re.match(resp).captured(1).toInt();
            if (pCount != 0) {
                query = "SELECT Full_name FROM Patients WHERE Doctor_id = " + QString::number(currentDocID);
                resp = m_sql->executeQueryBlock(query);
                re.setPattern("Full_name=(.*)");
                auto matches = re.globalMatch(resp);
                while (matches.hasNext()) {
                    match = matches.next();
                    m_container += match.captured(1) + "\n";
                }
            }
            else m_container += "У врача нет больных\n";
        }
    }
    else { //RT_DISEASES
        m_container += "Отчёт о болезнях от " + QDate::currentDate().toString() + " " + QTime::currentTime().toString() +"\n";
        QString query = "SELECT COUNT(*) FROM Diseases";
        re.setPattern("COUNT\\(\\*\\)=(\\d+)");
        int dCount = re.match(m_sql->executeQueryBlock(query)).captured(1).toInt();
        m_container += "Количество болезней: " + QString::number(dCount) + "\n";
        query = "SELECT * FROM Diseases";
        auto resp = m_sql->executeQueryBlock(query);
        RegexSplitter s;
        s.beg_part = "ID=";
        QStringList splittedResp = s(resp);
        re.setPatternOptions(re.patternOptions() | QRegularExpression::PatternOption::DotMatchesEverythingOption);
        for (auto & currMatch : splittedResp) {
            re.setPattern("ID=(\\d+)\nName=(.*)\nSymptoms=([\\s\\S]*?)\nCure=([\\s\\S]*)");
            auto match = re.match(currMatch);
            m_container += QString(20,'=');
            m_container += "\nНазвание болезни: " + match.captured(2) + "\n";
            m_container += "Симптомы болезни: " + match.captured(3) + "\n";
            m_container += "Лечение болезни: " + match.captured(4) + "\n";
            query = "SELECT COUNT(*) From Patients WHERE Disease_id = " + match.captured(1);
            re.setPattern("COUNT\\(\\*\\)=(\\d+)");
            auto pCount = re.match(m_sql->executeQueryBlock(query)).captured(1);
            m_container += "Количество больных, страдающих от болезни: " + pCount + "\n";
            if (pCount.toInt() > 0) {
                m_container += "Список больных:\n";
                query = "SELECT Full_name From Patients WHERE Disease_id = " + match.captured(1);
                re.setPattern("Full_name=(.*)");
                auto names = re.globalMatch(m_sql->executeQueryBlock(query));
                while (names.hasNext()) {
                    auto _m = names.next();
                    m_container += _m.captured(1) + "\n";
                }
            }
        }
    }
}
