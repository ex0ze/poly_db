#include "AddPatientDlg.h"
#include "ui_AddPatientDlg.h"

AddPatientDlg::AddPatientDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddPatientDlg)
{
    ui->setupUi(this);
}

void AddPatientDlg::setDiseaseMap(const QMap<QString, int> &map)
{
    m_diseaseMap = map;
    for (auto it = m_diseaseMap.begin(); it != m_diseaseMap.end(); ++it) {
        ui->diseaseCombobox->addItem(it.key());
    }
}

void AddPatientDlg::accept()
{
    if (ui->fullNameEdit->text().isEmpty() || ui->addressEdit->text().isEmpty()) {
        QMessageBox::warning(nullptr,"Ошибка ввода", "ФИО и адрес не могут быть пустыми.");
    }
    else QDialog::accept();
}

void AddPatientDlg::getData(QString &patient_name, QString &patient_address, QString &disease_date, int &disease_id)
{
    patient_name = ui->fullNameEdit->text();
    patient_address = ui->addressEdit->text();
    disease_date = ui->diseaseDateEdit->date().toString();
    disease_id = (ui->diseaseCombobox->currentIndex() == 0) ? 0 : m_diseaseMap[ui->diseaseCombobox->currentText()];
}

AddPatientDlg::~AddPatientDlg()
{
    delete ui;
}
