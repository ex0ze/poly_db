#include "DBConnectDialog.h"
#include "ui_DBConnectDialog.h"

DBConnectDialog::DBConnectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DBConnectDialog)
{
    ui->setupUi(this);
    connect(ui->addressMode,QOverload<int>::of(&QComboBox::activated),[&](int idx){
        if (idx == 0) {
            ui->addressEdit->setEnabled(false);
        }
        else ui->addressEdit->setEnabled(true);
    });
}

void DBConnectDialog::getParam(QString &addr, int& port, int& mode)
{
    if (ui->addressMode->currentIndex() == 0) addr = "127.0.0.1";
    else addr = ui->addressEdit->text();
    port = ui->portSpinbox->value();
    mode = ui->workMode->currentIndex();
}

DBConnectDialog::~DBConnectDialog()
{
    delete ui;
}
