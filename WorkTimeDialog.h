#ifndef WORKTIMEDIALOG_H
#define WORKTIMEDIALOG_H

#include <QDialog>
#include <QMessageBox>
namespace Ui {
class WorkTimeDialog;
}

class WorkTimeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WorkTimeDialog(QWidget *parent = nullptr);
    QString getData() const;
    void accept() override;
    ~WorkTimeDialog();

private:
    Ui::WorkTimeDialog *ui;
    QString data;
};

#endif // WORKTIMEDIALOG_H
