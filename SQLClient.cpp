#include "SQLClient.h"

SQLClient::SQLClient(QObject *parent) : QObject(parent)
{
    m_socket = new QTcpSocket(this);
    connect(m_socket,&QTcpSocket::readyRead,this,&SQLClient::slotReadData);
    connect(m_socket,&QTcpSocket::connected,this,&SQLClient::slotOnConnect);
    connect(m_socket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),this,&SQLClient::slotOnError);
    connect(m_socket,&QTcpSocket::disconnected,this,&SQLClient::slotOnDisconnect);
}

QString SQLClient::executeQueryBlock(const QString &query)
{
    m_socket->write(query.toUtf8());
    bool rc = m_socket->waitForReadyRead();
    if (rc) return executeResult();
    else return QString();
}

void SQLClient::executeQueryNonblock(const QString &query)
{
    if (m_socket->state() == QAbstractSocket::SocketState::ConnectedState) {
        m_socket->write(query.toUtf8());
    }
}

bool SQLClient::isConnected() const
{
    while (m_socket->state() == QAbstractSocket::ConnectingState) {
        QApplication::processEvents();
    }
    return (m_socket->state() == QAbstractSocket::ConnectedState);
}

QString SQLClient::executeResult()
{
    auto ret = gotData.toUtf8();
    gotData.clear();
    return ret;
}

bool SQLClient::initConnection(const QString &addr, int port)
{
    m_socket->connectToHost(addr,port);
    return true;
}

bool SQLClient::initDisconnection()
{
    m_socket->disconnectFromHost();
    return true;
}

void SQLClient::slotReadData()
{
    while (m_socket->bytesAvailable() > 0) {
        gotData.append(m_socket->readAll());
    }
    emit dataReady();
}

void SQLClient::slotOnConnect()
{
    emit sendLog(QString("Connected to ") + m_socket->peerAddress().toString()+QString(":")+QString::number(m_socket->peerPort()));
}

void SQLClient::slotOnDisconnect()
{
    emit sendLog(QString("Disconnected"));
}

void SQLClient::slotOnError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::critical(nullptr, tr("БД"),
                              tr("Не найден сервер. Проверьте адрес и порт."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::critical(nullptr, tr("БД"),
                              tr("Отказано в подключении. Убедитесь в том, что сервер запущен."));
        break;
    default:
        QMessageBox::critical(nullptr, tr("БД"),
                              tr("Ошибка: %1.")
                              .arg(m_socket->errorString()));
    }
}
