#ifndef DISEASELISTWIDGET_H
#define DISEASELISTWIDGET_H

#include <QObject>
#include "DBListWidget.h"
#include "AddDiseaseDlg.h"

class DiseaseListWidget : public DBListWidget
{
    Q_OBJECT
public:
    explicit DiseaseListWidget(QWidget* parent = nullptr);
public slots:
    void updateList() override;
private slots:
    void showInfo() override;
    void showEditDisease();
private:
    const QString m_diseaseInfoRegex = "ID=(\\d+)\nName=(.*)\nSymptoms=([\\s\\S]*?)\nCure=([\\s\\S]*)";
    const QString m_updateRegex = "Name=(.*)";
    QAction* editDisease;
};

#endif // DISEASELISTWIDGET_H
