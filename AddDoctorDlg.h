#ifndef ADDDOCTORDLG_H
#define ADDDOCTORDLG_H

#include <QDialog>
#include <QMessageBox>
#include "WorkTimeDialog.h"
namespace Ui {
class AddDoctorDlg;
}

class AddDoctorDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AddDoctorDlg(QWidget *parent = nullptr);
    void accept() override;
    void getData(QString& docName, QString& cabNum, QString& polyNum, QString& workTime);
    ~AddDoctorDlg();

private slots:
    void on_setWorkTimeBtn_clicked();

private:
    Ui::AddDoctorDlg *ui;
};

#endif // ADDDOCTORDLG_H
