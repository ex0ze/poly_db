#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMdiSubWindow>
#include <QMap>
#include "DBConnectDialog.h"
#include "SQLClient.h"
#include "AddDoctorDlg.h"
#include "AddDiseaseDlg.h"
#include "AddPatientDlg.h"
#include "DocListWidget.h"
#include "PatientListWidget.h"
#include "DiseaseListWidget.h"
#include "ReportViewer.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void showDbConnectDlg();
    void updateStatusBar(const QString& data);
    void updateMode();
private slots:
    void on_actAddDoc_triggered();

    void on_actAddDisease_triggered();

    void on_actShowDocs_triggered();

    void on_actAddPatient_triggered();

    void on_actShowDiseases_triggered();

    void on_actShowPatients_triggered();

    void on_actReportPatients_triggered();

    void on_actReportDoctors_triggered();

    void on_actReportDiseases_triggered();

    void genReport(ReportFormer::ReportType type);
    void on_actDisconnect_triggered();

signals:
    void updateDoctors();
    void updateDiseases();
    void updatePatients();
private:
    Ui::MainWindow *ui;
    SQLClient* sql;
    int wMode;
};
#endif // MAINWINDOW_H
