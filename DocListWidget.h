#ifndef DOCLISTWIDGET_H
#define DOCLISTWIDGET_H

#include <QObject>
#include <QSet>
#include <QMap>
#include "DBListWidget.h"

class DocListWidget : public DBListWidget
{
    Q_OBJECT
public:
    explicit DocListWidget(QWidget *parent = nullptr);
public slots:
    void updateList() override;
private slots:
    void showInfo() override;
    void actRemoveDoc();
    void actShowPatientList();
private:
    const QString m_docInfoRegex = "ID=(\\d+)\nFull_name=(.*)\nCab_num=(.*)\nPoly_num=(.*)\nWork_time=(.*)";
    const QString m_updateRegex = "Full_name=(.*)";
    QAction* removeDoc;
    QAction* showPatientList;
};

#endif // DOCLISTWIDGET_H
