#include "DocClarifier.h"
#include "ui_DocClarifier.h"

DocClarifier::DocClarifier(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DocClarifier)
{
    ui->setupUi(this);
}

void DocClarifier::setCurrentDocName(const QString &name)
{
    ui->currentDocName->setText(name);
}

void DocClarifier::setDocMap(const QMap<int, QString> &map)
{
    docMap = map;
    for (auto &i:docMap) {
        ui->newDocCombobox->addItem(i);
    }
}

void DocClarifier::getData(int &newDocId)
{
    if (ui->newDocCombobox->currentIndex() == 0) newDocId = 0;
    else newDocId = docMap.key(ui->newDocCombobox->currentText());
}

DocClarifier::~DocClarifier()
{
    delete ui;
}
