#include "AddDoctorDlg.h"
#include "ui_AddDoctorDlg.h"

AddDoctorDlg::AddDoctorDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDoctorDlg)
{
    ui->setupUi(this);
}

void AddDoctorDlg::accept()
{
    if (ui->cabNumLabel->text().isEmpty()) {
        QMessageBox::warning(nullptr,"Ошибка ввода", "Поле 'Номер кабинета' не может быть пустым.");
        return;
    }
    if (ui->polyNumLabel->text().isEmpty()) {
        QMessageBox::warning(nullptr,"Ошибка ввода", "Поле 'Номер участка' не может быть пустым.");
        return;
    }
    if (ui->fullNameLabel->text().isEmpty()) {
        QMessageBox::warning(nullptr,"Ошибка ввода", "Поле 'ФИО доктора' не может быть пустым.");
        return;
    }
    if (ui->workTimeEdit->toPlainText().isEmpty()) {
        QMessageBox::warning(nullptr,"Ошибка ввода", "Задайте доктору время работы!");
        return;
    }
    QDialog::accept();
}

void AddDoctorDlg::getData(QString &docName, QString &cabNum, QString &polyNum, QString &workTime)
{
    docName = ui->fullNameLabel->text();
    cabNum = ui->cabNumLabel->text();
    polyNum = ui->polyNumLabel->text();
    workTime = ui->workTimeEdit->toPlainText();
}

AddDoctorDlg::~AddDoctorDlg()
{
    delete ui;
}

void AddDoctorDlg::on_setWorkTimeBtn_clicked()
{
    WorkTimeDialog dlg;
    if (QDialog::Accepted == dlg.exec()) {
        ui->workTimeEdit->setPlainText(dlg.getData());
    }
}
