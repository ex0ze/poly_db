#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    sql = new SQLClient(this);
    connect(ui->actConnect,&QAction::triggered,this,&MainWindow::showDbConnectDlg);
    setWindowIcon(QIcon(":/ico/main.png"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showDbConnectDlg()
{
    DBConnectDialog dlg;
    if (QDialog::Accepted == dlg.exec()) {
        QString addr;
        int port;
        dlg.getParam(addr, port, wMode);
        updateStatusBar("Установка соединения с БД...");
        sql->initConnection(addr,port);
        if (!sql->isConnected()) {
            QMessageBox::critical(nullptr, "Ошибка", "Ошибка соединения с БД");
            return;
        }
        updateStatusBar("Соединение с БД установлено");
        ui->menuDoctors->setEnabled(true);
        ui->menuReports->setEnabled(true);
        ui->menuDiseases->setEnabled(true);
        ui->menuPatients->setEnabled(true);
        ui->actDisconnect->setEnabled(true);
        ui->actConnect->setEnabled(false);
        updateMode();
    }
}

void MainWindow::updateStatusBar(const QString &data)
{
    ui->statusBar->showMessage(data,5000);
}

void MainWindow::updateMode()
{
    ui->actAddDoc->setEnabled(wMode == 1);
    ui->actAddDisease->setEnabled(wMode == 1);
    ui->actAddPatient->setEnabled(wMode == 1);
}


void MainWindow::on_actAddDoc_triggered()
{
    AddDoctorDlg dlg;
    if (QDialog::Accepted == dlg.exec()) {
        QString name, cab, poly, wtime;
        dlg.getData(name,cab,poly,wtime);
        QString query = "INSERT INTO Doctors(Full_name, Cab_num, Poly_num, Work_time) VALUES('";
        query += name + "','" + cab +"','" + poly + "','" + wtime +"');";
        sql->executeQueryBlock(query.toUtf8());
        updateStatusBar("Доктор добавлен");
        emit updateDoctors();
    }
}

void MainWindow::on_actAddDisease_triggered()
{
    AddDiseaseDlg dlg;
    if (QDialog::Accepted == dlg.exec()) {
        QString dname, dsym, dcure;
        dlg.getData(dname,dsym,dcure);
        QString query = "INSERT INTO Diseases(Name, Symptoms, Cure) VALUES('";
        query += dname +"','" + dsym + "','" + dcure + "');";
        sql->executeQueryBlock(query.toUtf8());
        updateStatusBar("Болезнь добавлена");
        emit updateDiseases();
    }
}

void MainWindow::on_actShowDocs_triggered()
{
    auto doclist = new DocListWidget();
    doclist->setAccessMode(wMode);
    QMdiSubWindow* subWindow = new QMdiSubWindow;
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWidget(doclist);
    ui->mdiArea->addSubWindow(subWindow);
    doclist->attachSqlClient(sql);
    connect(this,&MainWindow::updatePatients,doclist,&DocListWidget::updateList);
    doclist->updateList();
    doclist->show();

}

void MainWindow::on_actAddPatient_triggered()
{
    QString query = "SELECT COUNT(*) FROM Doctors";
    auto resp = sql->executeQueryBlock(query);
    QRegularExpression re("COUNT\\(\\*\\)=(\\d+)");
    auto match = re.match(resp);
    bool ok = true;
    if (match.captured(1).toInt(&ok) == 0 || !ok) {
        QMessageBox::warning(nullptr,"Ошибка добавления пациента","В поликлинике должен работать хотя бы один врач!");
        return;
    }
    QMap<int,int> doc_map; //ключ - айди доктора, значение - количество его пациентов
    query = "SELECT ID FROM Doctors";
    resp = sql->executeQueryBlock(query);
    re.setPattern("ID=(\\d*)");
    auto matches = re.globalMatch(resp);
    while (matches.hasNext()) {
        match = matches.next();
        doc_map[match.captured(1).toInt()] = 0;
    }
    re.setPattern("COUNT\\(\\*\\)=(\\d+)");
    for (auto it = doc_map.begin(); it != doc_map.end(); ++it) {
        query = "SELECT COUNT(*) FROM Patients WHERE Doctor_id = " + QString::number(it.key());
        resp = sql->executeQueryBlock(query);
        match = re.match(resp);
        it.value() = match.captured(1).toInt();
    }
    auto cxxMap = doc_map.toStdMap();
    int docId = (std::min_element(cxxMap.begin(),cxxMap.end(),
                                  [&](const std::pair<int,int>& lhs, const std::pair<int,int>& rhs) -> bool {
        return lhs.second < rhs.second;
    }))->first;
    QMap<QString,int> disease_map; //название болезни -
    query = "SELECT ID,Name FROM Diseases";
    resp = sql->executeQueryBlock(query);
    re.setPattern("ID=(\\d+)\nName=(.*)");
    matches = re.globalMatch(resp);
    while (matches.hasNext()) {
        auto _m = matches.next();
        ok = true;
        disease_map[_m.captured(2)] = _m.captured(1).toInt(&ok);
        if (!ok) {
            QMessageBox::critical(nullptr,"Ошибка считывания БД","Входные данные имеют неверный формат");
        }
    }
    AddPatientDlg dlg;
    dlg.setDiseaseMap(disease_map);
    if (QDialog::Accepted == dlg.exec()) {
        QString full_name, address, date;
        int id;
        dlg.getData(full_name, address, date,id);
        query = "INSERT INTO Patients(Full_name, Address, Disease_time, Disease_id, Doctor_id) VALUES('";
        query += full_name + "','" + address + "','" + date +"'," + ((id == 0)? "NULL" : QString::number(id)) + "," + QString::number(docId) + ");";
        sql->executeQueryBlock(query);
        updateStatusBar("Пациент добавлен");
        emit updatePatients();
    }
}

void MainWindow::on_actShowDiseases_triggered()
{
    auto diseaselist = new DiseaseListWidget();
    diseaselist->setAccessMode(wMode);
    QMdiSubWindow* subWindow = new QMdiSubWindow;
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWidget(diseaselist);
    ui->mdiArea->addSubWindow(subWindow);
    diseaselist->attachSqlClient(sql);
    connect(this,&MainWindow::updateDiseases,diseaselist,&DiseaseListWidget::updateList);
    diseaselist->updateList();
    diseaselist->show();
}

void MainWindow::on_actShowPatients_triggered()
{
    auto patientList = new PatientListWidget(this);
    patientList->setAccessMode(wMode);
    QMdiSubWindow* subWindow = new QMdiSubWindow;
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWidget(patientList);
    ui->mdiArea->addSubWindow(subWindow);
    patientList->attachSqlClient(sql);
    connect(this,&MainWindow::updatePatients,patientList,&PatientListWidget::updateList);
    patientList->updateList();
    patientList->show();
}

void MainWindow::on_actReportPatients_triggered()
{
    genReport(ReportFormer::ReportType::RT_PATIENTS);
    updateStatusBar("Создан отчёт о пациентах");
}

void MainWindow::on_actReportDoctors_triggered()
{
    genReport(ReportFormer::ReportType::RT_DOCTORS);
    updateStatusBar("Создан отчёт о врачах");
}

void MainWindow::on_actReportDiseases_triggered()
{
    genReport(ReportFormer::ReportType::RT_DISEASES);
    updateStatusBar("Создан отчёт о болезнях");
}

void MainWindow::genReport(ReportFormer::ReportType type)
{
    ReportViewer* rv = new ReportViewer(this);
    rv->attachSql(sql);
    rv->generateReport(type);
    QMdiSubWindow* subWindow = new QMdiSubWindow;
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->setWidget(rv);
    ui->mdiArea->addSubWindow(subWindow);
    rv->show();
}

void MainWindow::on_actDisconnect_triggered()
{
    ui->mdiArea->closeAllSubWindows();
    sql->initDisconnection();
    ui->menuDoctors->setEnabled(false);
    ui->menuReports->setEnabled(false);
    ui->menuDiseases->setEnabled(false);
    ui->menuPatients->setEnabled(false);
    ui->actDisconnect->setEnabled(false);
    ui->actConnect->setEnabled(true);
    updateStatusBar("Соединение с БД разорвано");
}
