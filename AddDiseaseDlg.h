#ifndef ADDDISEASEDLG_H
#define ADDDISEASEDLG_H

#include <QDialog>
#include <QMessageBox>

namespace Ui {
class AddDiseaseDlg;
}

class AddDiseaseDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AddDiseaseDlg(QWidget *parent = nullptr);
    void getData(QString& name, QString& symptoms, QString& cure);
    void setData(const QString& name, const QString& symptoms, const QString& cure);
    void accept() override;
    ~AddDiseaseDlg();

private:
    Ui::AddDiseaseDlg *ui;
};

#endif // ADDDISEASEDLG_H
