#ifndef SQLCLIENT_H
#define SQLCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QMessageBox>
#include <QApplication>
#include <QHostAddress>

class SQLClient : public QObject
{
    Q_OBJECT
public:
    explicit SQLClient(QObject *parent = nullptr);
    QString executeQueryBlock(const QString& query);
    void executeQueryNonblock(const QString& query);
    bool isConnected() const;
    QString executeResult();
signals:
    void sendLog(const QString&);
    void dataReady();
public slots:
    bool initConnection(const QString& addr, int port);
    bool initDisconnection();
private slots:
    void slotReadData();
    void slotOnConnect();
    void slotOnDisconnect();
    void slotOnError(QAbstractSocket::SocketError socketError);
private:
    QTcpSocket* m_socket;
    QString gotData;
};

#endif // SQLCLIENT_H
