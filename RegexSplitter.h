#ifndef REGEXSPLITTER_H
#define REGEXSPLITTER_H
#include <QRegularExpression>
#include <QString>
#include <QStringList>
struct RegexSplitter {
    QString beg_part;
    QStringList operator()(QString subject) {
        QStringList res;
        int idx = 1;
        while (idx != -1) {
            idx = subject.indexOf(beg_part,1);
            res << subject.left(idx);
            subject = subject.right(subject.size() - res.back().size());
        }
        return res;
    }
};


#endif // REGEXSPLITTER_H
