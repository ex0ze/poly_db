#ifndef DOCCLARIFIER_H
#define DOCCLARIFIER_H

#include <QDialog>
#include <QMap>

namespace Ui {
class DocClarifier;
}

class DocClarifier : public QDialog
{
    Q_OBJECT

public:
    explicit DocClarifier(QWidget *parent = nullptr);
    void setCurrentDocName(const QString& name);
    void setDocMap(const QMap<int, QString>& map);
    void getData(int& newDocId);
    ~DocClarifier();

private:
    Ui::DocClarifier *ui;
    QMap<int, QString> docMap;
};

#endif // DOCCLARIFIER_H
