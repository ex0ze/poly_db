#include "DiseaseListWidget.h"

DiseaseListWidget::DiseaseListWidget(QWidget *parent) : DBListWidget(parent)
{
    connect(this,&QListWidget::customContextMenuRequested,this,&DiseaseListWidget::showContextMenu);
    m_actShowInfo->setText("Показать инфо о болезни");
    connect(m_actShowInfo,&QAction::triggered,this,&DiseaseListWidget::showInfo);
    m_contextMenu->addAction(m_actShowInfo);
    editDisease = new QAction("Изменить свойства болезни", this);
    connect(editDisease, &QAction::triggered, this, &DiseaseListWidget::showEditDisease);
    m_contextMenu->addAction(editDisease);
    setWindowTitle("Список болезней");
}

void DiseaseListWidget::updateList()
{
    editDisease->setEnabled(m_accessMode == 1);
    clear();
    QString query = "SELECT Name FROM Diseases";
    QString resp = m_sql->executeQueryBlock(query);
    QRegularExpression re(m_updateRegex);
    auto matches = re.globalMatch(resp);
    QStringList dlist;
    while (matches.hasNext()) {
        auto match = matches.next();
        dlist << match.captured(1);
    }
    setList(dlist);
}

void DiseaseListWidget::showInfo()
{
    QString query = "SELECT * FROM Diseases WHERE Name = '" + currentItem()->text() + "'";
    QString resp = m_sql->executeQueryBlock(query);
    QRegularExpression re(m_diseaseInfoRegex);
    auto match = re.match(resp);
    if (match.hasMatch()) {
        QString data = "Идентификатор: " + match.captured(1) + "\n";
        data += "Название болезни: " + match.captured(2) + "\n";
        data += "Симптомы болезни: " + match.captured(3) + "\n";
        data += "Лекарство от болезни: " + match.captured(4);
        QMessageBox::information(nullptr,"Инфо о болезни", data);
    }
}

void DiseaseListWidget::showEditDisease()
{
    QString query = "SELECT * FROM Diseases WHERE Name = '" + currentItem()->text() + "'";
    QString resp = m_sql->executeQueryBlock(query);
    QRegularExpression re(m_diseaseInfoRegex);
    auto match = re.match(resp);
    QString name = match.captured(2);
    QString symptoms = match.captured(3);
    QString cure = match.captured(4);
    AddDiseaseDlg dlg;
    dlg.setData(name,symptoms,cure);
    if (QDialog::Accepted == dlg.exec()) {
        dlg.getData(name,symptoms,cure);
        query = "UPDATE Diseases SET Name = '" + name +"', Symptoms = '" +symptoms + "', Cure = '" + cure + "' WHERE ID = " + match.captured(1);
        m_sql->executeQueryBlock(query);
        QMessageBox::information(nullptr,"Успех", "Обновление данных о болезни " + currentItem()->text() + " успешно");
        updateList();
    }
}
