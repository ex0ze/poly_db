#include "DocListWidget.h"

DocListWidget::DocListWidget(QWidget *parent) : DBListWidget(parent)
{
    connect(this,&QListWidget::customContextMenuRequested,this,&DocListWidget::showContextMenu);
    m_actShowInfo->setText("Показать инфо о враче");
    connect(m_actShowInfo,&QAction::triggered,this,&DocListWidget::showInfo);
    m_contextMenu->addAction(m_actShowInfo);
    showPatientList = new QAction("Показать список пациентов у врача",this);
    connect(showPatientList, &QAction::triggered, this, &DocListWidget::actShowPatientList);
    m_contextMenu->addAction(showPatientList);
    removeDoc = new QAction("Уволить врача", this);
    connect(removeDoc, &QAction::triggered, this, &DocListWidget::actRemoveDoc);
    m_contextMenu->addAction(removeDoc);
    setWindowTitle("Список врачей");
}


void DocListWidget::showInfo()
{
    QString query = "SELECT * FROM Doctors WHERE Full_name = '" + currentItem()->text() + "'";
    QString resp = m_sql->executeQueryBlock(query);
    QRegularExpression re(m_docInfoRegex);
    auto match = re.match(resp);
    if (match.hasMatch()) {
        QString data = "Идентификатор: " + match.captured(1) + "\n";
        data += "Имя врача: " + match.captured(2) + "\n";
        data += "Номер кабинета: " + match.captured(3) + "\n";
        data += "Номер поликлиники: " + match.captured(4) + "\n";
        data += "Время работы врача: " + match.captured(5) + "\n";
        QMessageBox::information(nullptr,"Инфо о враче", data);
    }
}

void DocListWidget::actRemoveDoc()
{
    if (currentItem() == nullptr) return;
    if (QMessageBox::Cancel ==
            QMessageBox::warning(nullptr, "Подтвердите действие",
                                 QString("Вы действительно хотите уволить врача ") + currentItem()->text() + " ?"))
        return;
    //получаем ID врача
    QString query = "SELECT * FROM Doctors WHERE Full_name = '" + currentItem()->text() + "'";
    QString resp = m_sql->executeQueryBlock(query);
    QRegularExpression re(m_docInfoRegex);
    auto match = re.match(resp);
    int currentDocID = match.captured(1).toInt();
    //получаем список пациентов врача
    QSet<int> currentDocPatients;
    query = "SELECT ID FROM Patients WHERE Doctor_id = " + QString::number(currentDocID);
    resp = m_sql->executeQueryBlock(query);
    re.setPattern("ID=(\\d+)");
    auto matches = re.globalMatch(resp);
    while (matches.hasNext()) {
        match = matches.next();
        currentDocPatients.insert(match.captured(1).toInt());
    }
    //получаем список других докторов и строим мапу айди врача - количество пациентов
    query = "SELECT ID FROM Doctors WHERE ID != " + QString::number(currentDocID);
    QMap<int, int> otherDoctors;
    resp = m_sql->executeQueryBlock(query);
    matches = re.globalMatch(resp);
    re.setPattern("COUNT\\(\\*\\)=(\\d+)");
    while (matches.hasNext()) {
        match = matches.next();
        int id = match.captured(1).toInt();
        QString q2 = "SELECT COUNT(*) FROM Patients WHERE Doctor_id = " + match.captured(1);
        auto r2 = m_sql->executeQueryBlock(q2);
        int count = re.match(r2).captured(1).toInt();
        otherDoctors[id] = count;
    }
    QMap<int,int> balancedPatients; //айди пациента - новое айди врача
    if (otherDoctors.size() > 0) { // если есть другие врачи - балансируем

        auto cxxMap = otherDoctors.toStdMap();
        for (auto &patientID : currentDocPatients) {
            int mfd = (std::min_element(cxxMap.begin(),cxxMap.end(),
                                        [&](const std::pair<int,int>& lhs, const std::pair<int,int>& rhs) -> bool {
                return lhs.second < rhs.second;
            }))->first;
            balancedPatients[patientID] = mfd;
            cxxMap[mfd]++;
        }
    }
    else { //иначе пациенты без привязанного врача
        for (const auto &i:currentDocPatients) {
            balancedPatients[i] = 0;
        }
    }
    //перепривязка пациентов
    for (auto p_id = balancedPatients.begin(); p_id != balancedPatients.end(); ++p_id) {
        query = "UPDATE Patients SET Doctor_id = " + (p_id.value() == 0 ? "NULL" : QString::number(p_id.value())) + " WHERE ID = " + QString::number(p_id.key());
        m_sql->executeQueryBlock(query);
    }
    //удаление врача
    query = "DELETE FROM Doctors WHERE Full_name = '" + currentItem()->text() + "'";
    if (m_sql->executeQueryBlock(query) == "Ok") {
        QMessageBox::information(nullptr, "Успешно", QString("Врач ") + currentItem()->text() + " успешно уволен");
    }
}

void DocListWidget::actShowPatientList()
{
    if (currentItem() == nullptr) return;
    QString query = "SELECT * FROM Doctors WHERE Full_name = '" + currentItem()->text() + "'";
    QString resp = m_sql->executeQueryBlock(query);
    QRegularExpression re(m_docInfoRegex);
    auto match = re.match(resp);
    int currentDocID = match.captured(1).toInt();
    query = "SELECT Full_name FROM Patients WHERE Doctor_id = " + QString::number(currentDocID);
    resp = m_sql->executeQueryBlock(query);
    re.setPattern("Full_name=(.*)");
    auto matches = re.globalMatch(resp);
    QString data;
    while (matches.hasNext()) {
        match = matches.next();
        data += match.captured(1) + "\n";
    }
    QMessageBox::information(nullptr, "Больные на лечении у " + currentItem()->text(), data.isEmpty() ? QString("У врача нет больных") : data);
}

void DocListWidget::updateList()
{
    removeDoc->setEnabled(m_accessMode == 1);
    clear();
    QString query = "SELECT Full_name FROM Doctors";
    QString resp = m_sql->executeQueryBlock(query);
    QRegularExpression re(m_updateRegex);
    auto matches = re.globalMatch(resp);
    QStringList dlist;
    while (matches.hasNext()) {
        auto match = matches.next();
        dlist << match.captured(1);
    }
    setList(dlist);
}
