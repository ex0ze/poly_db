#ifndef REPORTVIEWER_H
#define REPORTVIEWER_H

#include <QWidget>
#include <QFile>
#include <QFileDialog>
#include "ReportFormer.h"
#include "SQLClient.h"
namespace Ui {
class ReportViewer;
}

class ReportViewer : public QWidget
{
    Q_OBJECT

public:
    explicit ReportViewer(QWidget *parent = nullptr);
    ~ReportViewer();
public slots:
    void generateReport(ReportFormer::ReportType type);
    void attachSql(SQLClient*);
private slots:
    void on_saveBtn_clicked();

private:
    Ui::ReportViewer *ui;
    ReportFormer rf;
};

#endif // REPORTVIEWER_H
