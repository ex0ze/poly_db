#ifndef DBCONNECTDIALOG_H
#define DBCONNECTDIALOG_H

#include <QDialog>
#include <QComboBox>

namespace Ui {
class DBConnectDialog;
}

class DBConnectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DBConnectDialog(QWidget *parent = nullptr);
    void getParam(QString& addr, int& port, int& mode);
    ~DBConnectDialog();

private:
    Ui::DBConnectDialog *ui;
};

#endif // DBCONNECTDIALOG_H
