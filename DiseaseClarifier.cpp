#include "DiseaseClarifier.h"
#include "ui_DiseaseClarifier.h"

DiseaseClarifier::DiseaseClarifier(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DiseaseClarifier)
{
    ui->setupUi(this);
}

void DiseaseClarifier::setDiseaseMap(const QMap<int, QString> &dmap)
{
    diseaseMap = dmap;
    for (const auto & it : dmap) {
        ui->diseaseCombobox->addItem(it);
    }
}

void DiseaseClarifier::getData(int &newDiseaseId)
{
    if (ui->diseaseCombobox->currentIndex() == 0) newDiseaseId = 0;
    else newDiseaseId = diseaseMap.key(ui->diseaseCombobox->currentText());
}

void DiseaseClarifier::setCurrentDisease(const QString &disease)
{
    ui->currentDiseaseLabel->setText(disease);
}

DiseaseClarifier::~DiseaseClarifier()
{
    delete ui;
}
