#ifndef DBLISTWIDGET_H
#define DBLISTWIDGET_H

#include <QObject>
#include <QWidget>
#include <QCloseEvent>
#include <QRegularExpression>
#include <QMenu>
#include <QListWidget>
#include <QMessageBox>
#include "SQLClient.h"

class DBListWidget : public QListWidget
{
    Q_OBJECT
public:
    explicit DBListWidget(QWidget* parent = nullptr);
    int accessMode() const;
    void setAccessMode(int accessMode);
signals:
    void shouldClose();
public slots:
    virtual void setList(const QStringList& list);
    virtual void updateList() = 0;

    void attachSqlClient(SQLClient*);
protected slots:
    void showContextMenu(const QPoint& pos);
    virtual void showInfo() = 0;
protected:
    QMenu* m_contextMenu;
    QAction* m_actShowInfo;
    SQLClient* m_sql;
    int m_accessMode;
    void closeEvent(QCloseEvent* event);
};

#endif // DBLISTWIDGET_H
