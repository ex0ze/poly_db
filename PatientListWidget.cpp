#include "PatientListWidget.h"

PatientListWidget::PatientListWidget(QWidget *parent) : DBListWidget(parent)
{
    connect(this,&QListWidget::customContextMenuRequested,this,&PatientListWidget::showContextMenu);
    m_actShowInfo->setText("Показать инфо о пациенте");
    connect(m_actShowInfo,&QAction::triggered,this,&PatientListWidget::showInfo);
    m_contextMenu->addAction(m_actShowInfo);
    removePatient = new QAction("Выписать пациента", this);
    connect(removePatient, &QAction::triggered, this, &PatientListWidget::actRemove);
    m_contextMenu->addAction(removePatient);
    reassignDoc = new QAction("Переназначить врача", this);
    connect(reassignDoc, &QAction::triggered, this, &PatientListWidget::actReassignDoc);
    m_contextMenu->addAction(reassignDoc);
    clarifyDisease = new QAction("Уточнить диагноз",this);
    connect(clarifyDisease, &QAction::triggered, this, &PatientListWidget::actClarify);
    m_contextMenu->addAction(clarifyDisease);
    aboutDisease = new QAction("Инфо о болезни пациента",this);
    connect(aboutDisease, &QAction::triggered, this, &PatientListWidget::actAboutDisease);
    m_contextMenu->addAction(aboutDisease);
    setWindowTitle("Список пациентов");
}

void PatientListWidget::updateList()
{
    clear();
    QString query = "SELECT Full_name FROM Patients";
    QString resp = m_sql->executeQueryBlock(query);
    QRegularExpression re(m_updateRegex);
    auto matches = re.globalMatch(resp);
    QStringList dlist;
    while (matches.hasNext()) {
        auto match = matches.next();
        dlist << match.captured(1);
    }
    setList(dlist);
}

void PatientListWidget::showInfo()
{
    QString query = "SELECT * FROM Patients WHERE Full_name = '" + currentItem()->text() + "'";
    QString resp = m_sql->executeQueryBlock(query);
    QRegularExpression re(m_patientInfoRegex);
    auto match = re.match(resp);
    if (match.hasMatch()) {
        QString data = "Идентификатор: " + match.captured(1) + "\n";
        data += "ФИО пациента: " + match.captured(2) + "\n";
        data += "Адрес пациента: " + match.captured(3) + "\n";
        data += "Дата заболевания: " + match.captured(4) + "\n";
        data += "Заболевание: ";
        query = "SELECT Name FROM Diseases WHERE ID = " + QString::number(match.captured(5).toInt());
        resp = m_sql->executeQueryBlock(query);
        re.setPattern("Name=(.*)");
        auto diseaseMatch = re.match(resp);
        if (diseaseMatch.hasMatch()) {
            data += diseaseMatch.captured(1);
        }
        else data += "Неизвестно";
        data += "\nНаходится на лечении у: ";
        query = "SELECT Full_name FROM Doctors WHERE ID = " + QString::number(match.captured(6).toInt());
        resp = m_sql->executeQueryBlock(query);
        re.setPattern(m_updateRegex);
        auto docMatch = re.match(resp);
        if (docMatch.hasMatch()) {
            data += docMatch.captured(1);
        }
        else data += "Доктор не назначен";
        QMessageBox::information(nullptr,"Инфо о пациенте", data);
    }
}

void PatientListWidget::actRemove()
{
    QString query = "DELETE FROM Patients WHERE Full_name = '" + currentItem()->text() + "'";
    auto resp = m_sql->executeQueryBlock(query);
    if (resp == "OK") {
        QMessageBox::information(nullptr, "Успешно", QString("Пациент ") + currentItem()->text() + " успешно выписан");
        updateList();
    }
}

void PatientListWidget::actAboutDisease()
{
    QString query = "SELECT Disease_id FROM Patients WHERE Full_name = '" + currentItem()->text() +"'";
    auto resp = m_sql->executeQueryBlock(query);
    QRegularExpression re("Disease_id=(\\d+)");
    int currentDiseaseId = re.match(resp).captured(1).toInt();
    query = "SELECT * FROM Diseases WHERE ID = " + QString::number(currentDiseaseId);
    resp = m_sql->executeQueryBlock(query);
    re.setPattern("ID=(\\d+)\nName=(.*)\nSymptoms=([\\s\\S]*?)\nCure=([\\s\\S]*)");
    auto match = re.match(resp);
    if (match.hasMatch()) {
        QString data = "Идентификатор: " + match.captured(1) + "\n";
        data += "Название болезни: " + match.captured(2) + "\n";
        data += "Симптомы болезни: " + match.captured(3) + "\n";
        data += "Лекарство от болезни: " + match.captured(4);
        QMessageBox::information(nullptr,"Инфо о болезни", data);
    }
    else {
        QMessageBox::information(nullptr,"Инфо о болезни", "Болезнь не была назначена");
    }
}

void PatientListWidget::actClarify()
{
    QString query = "SELECT Disease_id FROM Patients WHERE Full_name = '" + currentItem()->text() +"'";
    auto resp = m_sql->executeQueryBlock(query);
    QRegularExpression re("Disease_id=(\\d+)");
    int currentDiseaseId = re.match(resp).captured(1).toInt();
    query = "SELECT Name FROM Diseases WHERE ID = " + QString::number(currentDiseaseId);
    resp = m_sql->executeQueryBlock(query);
    re.setPattern("Name=(.*)");
    QString currentDiseaseName = re.match(resp).captured(1);
    QMap<int, QString> diseaseMap;
    query = "SELECT ID, Name FROM Diseases WHERE Name != '" + currentDiseaseName + "'";
    resp = m_sql->executeQueryBlock(query);
    re.setPattern("ID=(\\d+)\nName=(.*)");
    auto matches = re.globalMatch(resp);
    while (matches.hasNext()) {
        auto match = matches.next();
        diseaseMap[match.captured(1).toInt()] = match.captured(2);
    }
    DiseaseClarifier dcl;
    dcl.setWindowTitle(QString("Уточнение диагноза пациента ") + currentItem()->text());
    dcl.setCurrentDisease(currentDiseaseName);
    dcl.setDiseaseMap(diseaseMap);
    if (QDialog::Accepted == dcl.exec()) {
        int newID;
        dcl.getData(newID);
        query = "UPDATE Patients SET Disease_id = " + (newID == 0 ? "NULL" : QString::number(newID)) + " WHERE Full_name = '" +
                currentItem()->text() + "'";
        auto resp = m_sql->executeQueryBlock(query);
        if (resp == "Ok") {
            QMessageBox::information(nullptr, "Успешно", "Диагноз успешно уточнён");
        }
    }
}

void PatientListWidget::actReassignDoc()
{
    QString query = "SELECT Doctor_id FROM Patients WHERE Full_name = '" + currentItem()->text() +"'";
    auto resp = m_sql->executeQueryBlock(query);
    QRegularExpression re("Doctor_id=(\\d+)");
    int currentDocId = re.match(resp).captured(1).toInt();
    query = "SELECT Full_name FROM Doctors WHERE ID = " + QString::number(currentDocId);
    resp = m_sql->executeQueryBlock(query);
    re.setPattern("Full_name=(.*)");
    QString currentDocName = re.match(resp).captured(1);
    if (currentDocName.isEmpty()) currentDocName = "Не назначено";
    QMap<int, QString> docMap;
    query = "SELECT ID, Full_name FROM Doctors WHERE Full_name != '" + currentDocName + "'";
    resp = m_sql->executeQueryBlock(query);
    re.setPattern("ID=(\\d+)\nFull_name=(.*)");
    auto matches = re.globalMatch(resp);
    while (matches.hasNext()) {
        auto match = matches.next();
        docMap[match.captured(1).toInt()] = match.captured(2);
    }
    DocClarifier dcl;
    dcl.setWindowTitle("Переназначение врача пациенту " + currentItem()->text());
    dcl.setCurrentDocName(currentDocName);
    dcl.setDocMap(docMap);
    if (QDialog::Accepted == dcl.exec()) {
        int newID;
        dcl.getData(newID);
        query = "UPDATE Patients SET Doctor_id = " + (newID == 0 ? "NULL" : QString::number(newID)) + " WHERE Full_name = '" +
                currentItem()->text() + "'";
        auto resp = m_sql->executeQueryBlock(query);
        if (resp == "Ok") {
            QMessageBox::information(nullptr, "Успешно", "Доктор успешно переназначен");
        }
    }
}
