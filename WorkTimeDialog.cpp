#include "WorkTimeDialog.h"
#include "ui_WorkTimeDialog.h"

WorkTimeDialog::WorkTimeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WorkTimeDialog)
{
    ui->setupUi(this);
}

QString WorkTimeDialog::getData() const
{
    return data;
}

void WorkTimeDialog::accept()
{
    data.clear();
    if (ui->monChk->checkState() == Qt::Checked) {
        data += "Пн. " + ui->monStart->time().toString() + "-" + ui->monEnd->time().toString() + ";";
    }
    if (ui->tueChk->checkState() == Qt::Checked) {
        data += "Вт. " + ui->tueStart->time().toString() + "-" + ui->tueEnd->time().toString() + ";";
    }
    if (ui->wedChk->checkState() == Qt::Checked) {
        data += "Ср. " + ui->wedStart->time().toString() + "-" + ui->wedEnd->time().toString() + ";";
    }
    if (ui->thuChk->checkState() == Qt::Checked) {
        data += "Чт. " + ui->thuStart->time().toString() + "-" + ui->thuEnd->time().toString() + ";";
    }
    if (ui->friChk->checkState() == Qt::Checked) {
        data += "Пт. " + ui->friStart->time().toString() + "-" + ui->friEnd->time().toString() + ";";
    }
    if (ui->satChk->checkState() == Qt::Checked) {
        data += "Сб. " + ui->satStart->time().toString() + "-" + ui->satEnd->time().toString() + ";";
    }
    if (ui->sunChk->checkState() == Qt::Checked) {
        data += "Вс. " + ui->sunStart->time().toString() + "-" + ui->sunEnd->time().toString() + ";";
    }
    if (!data.isEmpty()) data.chop(1);
    else {
        QMessageBox::critical(nullptr, "Ошибка", "Врач должен работать хотя бы один день в неделю");
        return;
    }
    QDialog::accept();
}

WorkTimeDialog::~WorkTimeDialog()
{
    delete ui;
}
