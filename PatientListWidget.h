#ifndef PATIENTLISTWIDGET_H
#define PATIENTLISTWIDGET_H

#include <QObject>
#include "DBListWidget.h"
#include "DiseaseClarifier.h"
#include "DocClarifier.h"

class PatientListWidget : public DBListWidget
{
    Q_OBJECT
public:
    explicit PatientListWidget(QWidget *parent = nullptr);
public slots:
    void updateList() override;
private slots:
    void showInfo() override;
    void actRemove();
    void actAboutDisease();
    void actClarify();
    void actReassignDoc();
private:
    const QString m_patientInfoRegex = "ID=(\\d+)\nFull_name=(.*)\nAddress=(.*)\nDisease_time=(.*)\nDisease_id=(.*)\nDoctor_id=(.*)\n";
    const QString m_updateRegex = "Full_name=(.*)";
    QAction* removePatient;
    QAction* clarifyDisease;
    QAction* aboutDisease;
    QAction* reassignDoc;
};

#endif // PATIENTLISTWIDGET_H
