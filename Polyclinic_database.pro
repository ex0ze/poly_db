QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AddDiseaseDlg.cpp \
    AddDoctorDlg.cpp \
    AddPatientDlg.cpp \
    DBConnectDialog.cpp \
    DBListWidget.cpp \
    DiseaseClarifier.cpp \
    DiseaseListWidget.cpp \
    DocClarifier.cpp \
    DocListWidget.cpp \
    PatientListWidget.cpp \
    ReportFormer.cpp \
    ReportViewer.cpp \
    SQLClient.cpp \
    WorkTimeDialog.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    AddDiseaseDlg.h \
    AddDoctorDlg.h \
    AddPatientDlg.h \
    DBConnectDialog.h \
    DBListWidget.h \
    DiseaseClarifier.h \
    DiseaseListWidget.h \
    DocClarifier.h \
    DocListWidget.h \
    PatientListWidget.h \
    RegexSplitter.h \
    ReportFormer.h \
    ReportViewer.h \
    SQLClient.h \
    WorkTimeDialog.h \
    mainwindow.h

FORMS += \
    AddDiseaseDlg.ui \
    AddDoctorDlg.ui \
    AddPatientDlg.ui \
    DBConnectDialog.ui \
    DiseaseClarifier.ui \
    DocClarifier.ui \
    ReportViewer.ui \
    WorkTimeDialog.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


RESOURCES += \
    res.qrc
