#ifndef DISEASECLARIFIER_H
#define DISEASECLARIFIER_H

#include <QDialog>
#include <QString>
#include <QMap>

namespace Ui {
class DiseaseClarifier;
}

class DiseaseClarifier : public QDialog
{
    Q_OBJECT

public:
    explicit DiseaseClarifier(QWidget *parent = nullptr);
    void setDiseaseMap(const QMap<int, QString>& dmap);
    void getData(int& newDiseaseId);
    void setCurrentDisease(const QString& disease);
    ~DiseaseClarifier();

private:
    Ui::DiseaseClarifier *ui;
    QMap<int, QString> diseaseMap;
};

#endif // DISEASECLARIFIER_H
