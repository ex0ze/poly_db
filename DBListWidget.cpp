#include "DBListWidget.h"

DBListWidget::DBListWidget(QWidget* parent) : QListWidget(parent)
{
    setContextMenuPolicy(Qt::CustomContextMenu);
    m_contextMenu = new QMenu(this);
    m_actShowInfo = new QAction(this);
}

void DBListWidget::setList(const QStringList &list)
{
    clear();
    insertItems(0,list);
}

void DBListWidget::attachSqlClient(SQLClient * s)
{
    m_sql = s;
}

void DBListWidget::showContextMenu(const QPoint &pos)
{
    if (currentItem() == nullptr) return;
    m_contextMenu->exec(mapToGlobal(pos));
}

int DBListWidget::accessMode() const
{
    return m_accessMode;
}

void DBListWidget::setAccessMode(int accessMode)
{
    m_accessMode = accessMode;
}

void DBListWidget::closeEvent(QCloseEvent *event)
{
    emit shouldClose();
    event->accept();
}
