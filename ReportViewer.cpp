#include "ReportViewer.h"
#include "ui_ReportViewer.h"

ReportViewer::ReportViewer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReportViewer)
{
    ui->setupUi(this);
}

ReportViewer::~ReportViewer()
{
    delete ui;
}

void ReportViewer::generateReport(ReportFormer::ReportType type)
{
    switch (type) {
    case ReportFormer::ReportType::RT_PATIENTS:
        ui->reportTypeLabel->setText("Отчёт о пациентах");
        break;
    case ReportFormer::ReportType::RT_DOCTORS:
        ui->reportTypeLabel->setText("Отчёт о врачах");
        break;
    case ReportFormer::ReportType::RT_DISEASES:
        ui->reportTypeLabel->setText("Отчёт о болезнях");
        break;
    }
    rf.createReport(type);
    ui->reportBrowser->setPlainText(rf.container());
}

void ReportViewer::attachSql(SQLClient * s)
{
    rf.setSql(s);
}

void ReportViewer::on_saveBtn_clicked()
{
    auto savePath = QFileDialog::getSaveFileName(nullptr, "Сохранение отчёта", QDir::currentPath(), "Текстовые файлы (*.txt)");
    QFile f(savePath);
    if (!f.open(QIODevice::OpenModeFlag::Text | QIODevice::OpenModeFlag::WriteOnly)) {
        QMessageBox::critical(nullptr, "Ошибка", "Невозможно сохранить файл");
        return;
    }
    f.write(rf.container().toUtf8());
    f.close();
}
