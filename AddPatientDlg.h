#ifndef ADDPATIENTDLG_H
#define ADDPATIENTDLG_H

#include <QDialog>
#include <QMessageBox>
#include <QString>
#include <QMap>

namespace Ui {
class AddPatientDlg;
}

class AddPatientDlg : public QDialog
{
    Q_OBJECT

public:
    explicit AddPatientDlg(QWidget *parent = nullptr);
    void setDiseaseMap(const QMap<QString,int>& map);
    void accept() override;
    void getData(QString& patient_name, QString& patient_address, QString& disease_date, int& disease_id);
    ~AddPatientDlg();

private:
    Ui::AddPatientDlg *ui;
    QMap<QString,int> m_diseaseMap;
};

#endif // ADDPATIENTDLG_H
