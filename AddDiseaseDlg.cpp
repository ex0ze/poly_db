#include "AddDiseaseDlg.h"
#include "ui_AddDiseaseDlg.h"

AddDiseaseDlg::AddDiseaseDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDiseaseDlg)
{
    ui->setupUi(this);
}

void AddDiseaseDlg::getData(QString &name, QString &symptoms, QString &cure)
{
    name = ui->diseaseNameEdit->text();
    symptoms = ui->diseaseSymptomsEdit->toPlainText();
    cure = ui->diseaseCureEdit->toPlainText();
}

void AddDiseaseDlg::setData(const QString &name, const QString &symptoms, const QString &cure)
{
    ui->diseaseNameEdit->setText(name);
    ui->diseaseSymptomsEdit->setPlainText(symptoms);
    ui->diseaseCureEdit->setPlainText(cure);
}

void AddDiseaseDlg::accept()
{
    if (ui->diseaseNameEdit->text().isEmpty() || ui->diseaseSymptomsEdit->toPlainText().isEmpty() || ui->diseaseCureEdit->toPlainText().isEmpty()) {
        QMessageBox::warning(nullptr,"Ошибка ввода", "Ни одно поле не может быть пустым");
        return;
    }
    QDialog::accept();
}

AddDiseaseDlg::~AddDiseaseDlg()
{
    delete ui;
}
